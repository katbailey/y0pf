eco = require 'eco'
fs = require 'fs'
resource = require 'express-resource'

exports.setupRoutes = ->
    app = this
    app.get '/', (req, res) ->
        template = fs.readFileSync __dirname + "/views/index.html.eco", "utf-8"
        res.send eco.render template, title: "Your Own Personal Flickr!"

    app.get '/services/rest', (req, res) ->
        console.log req.query
        # do stuff based on the method param in the query

    app.resource 'services/rest/photos', require './resources/photos'

    app.resource 'admin/users', require './resources/users'