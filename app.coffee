port = process.env.PORT ? 3000

if process.argv.indexOf('--seed') > -1
  GLOBAL.db_seed = true

if process.argv.indexOf('--no-auth') > -1
  GLOBAL.no_auth = true


express = require 'express'
mongoose = require 'mongoose'
# config.coffee holds database parameters, access credentials, allowed filenames
config = require './config'
loginCredentials = config.username + ":" + config.password
dbUrl = config.databaseUrl
dbName = config.databaseName
app = module.exports = express.createServer()

initializeDb = ->
  console.log 'attempting to connect to mongodb'
  mongoose.connection.on 'open', ->
    console.log 'Connected to MongoDB successfully!'
  mongoose.connect "mongodb://" + loginCredentials + "@" + dbUrl + "/" + dbName

app.configure ->
  app.set 'views', __dirname + '/views'
  app.set 'view engine', 'eco'
  
  app.use express.bodyParser()
  app.use express.cookieParser()
  # app.use express.session({ secret: "keyboard cat" })
  app.use express.methodOverride()
  app.use app.router
  app.use express.static __dirname + '/public'
  app.use express.basicAuth (username, password) -> 
    username is "krazynode" and password is "srsly"
  
  app.enable 'serve jquery', 'default layout'

routes = require './routes'
models = require './models'

routes.setupRoutes.call app
GLOBAL.models = models

initializeDb()

app.listen 3000
console.log "Express server listening on port %d in %s mode",
  app.address().port, app.settings.env
