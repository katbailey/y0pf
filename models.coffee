mongoose = require('mongoose')
Schema = mongoose.Schema
ObjectId = Schema.ObjectId

CommentSchema = new Schema({
    title     : String
  , body      : String
  , date      : Date
})

PhotoSchema = new Schema({
    author    : ObjectId
  , title     : String
  , body      : String
  , buf       : Buffer
  , date      : Date
  , comments  : [CommentSchema]
  , meta      : {
      votes : Number
    , favs  : Number
  }
})

exports.Comment = mongoose.model('Comment', CommentSchema)
exports.Photo = mongoose.model('Photo', PhotoSchema)

