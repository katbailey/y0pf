This project is an initial attempt to reproduce the set-up described here
http://fabianosoriani.wordpress.com/2011/08/15/express-api-on-node-js-with-mysql-auth/
using CoffeeScript.

It's currently set up to work with mysql but I've added the initial code for
getting it to talk to a MongoDB backend.

The only models set up so far are photos and users. Authentication to come ;-)